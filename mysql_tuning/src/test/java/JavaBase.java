import java.nio.charset.StandardCharsets;

/**
 * @Author Atu
 * @Date 2024/6/23 9:10
 * @Version 1.0
 */
public class JavaBase {
    public static void main(String[] args) {
        String msg = "Hello, SockeBIO!";
        System.out.println(msg.length());
        System.out.println(msg.getBytes(StandardCharsets.UTF_8).length);
        System.out.println(msg.getBytes(StandardCharsets.UTF_8));
    }
}
