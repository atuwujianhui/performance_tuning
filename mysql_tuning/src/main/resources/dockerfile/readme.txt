# 1. 构建 mysql_tuning 镜像
docker build -t mysql_tuning:1.0.0 .
# 创建 Docker 网络
docker network create mysql_tuning_network
docker network create --subnet 172.18.0.0/16 --gateway 172.18.1.1 mysql_tuning_network

# 2. （1）运行 mysql:8.0，端口 3306；（2）拷贝数据库初始化文件到容器中；（3）初始化数据库
（1） docker run --name mysql8 --network mysql_tuning_network -e MYSQL_ROOT_PASSWORD=root.123456 -d -p 3306:3306 mysql:8.0
（2） docker cp 99_mysql_init.sql mysql8:/
（3） docker exec mysql8 /bin/sh -c 'mysql -uroot -proot.123456 < /99_mysql_init.sql'

# 3. 运行 mysql_tuning，端口 8080
docker run --name mysql_tuning --network mysql_tuning_network -d -p 8080:8080 mysql_tuning

# 连接本地数据库
java -jar -Dspring.datasource.url="jdbc:mysql://localhost:3306/performance_tuning?useUnicode=true&characterEncoding=utf-8&useSSL=false" \
    mysql_tuning-1.0-SNAPSHOT.jar \
    --spring.datasource.username="root" \
    --spring.datasource.password="root.123456" \
    --spring.datasource.driver-class-name="com.mysql.cj.jdbc.Driver"

# 连接腾讯云数据库
java -jar -Dspring.datasource.url="jdbc:mysql://gz-cdb-035ob96p.sql.tencentcdb.com:24482/performance_tuning?useUnicode=true&characterEncoding=utf-8&useSSL=false" \
    mysql_tuning.jar \
    --spring.datasource.username="root" \
    --spring.datasource.password="root.123456"

"172.18.0.1/16"
# 4. 使用 docker network connect 命令将 mysql_tuning 加入到 mysql 的网络，这样就可以使用 localhost:3306 连接 MySQL。
docker network connect mysql_tuning mysql

#############################################

# 1. 获取第一条记录
curl http://localhost:8080/payment/get/1
# 2. 创建 1 条记录，第一条记录的 ID 为 1
curl http://localhost:8080/payment/autoCreate
# 3. 获取第一条记录
curl http://localhost:8080/payment/get/1

# 4. 根据字段 “C” 更新 “T_NO_INDEX” 表，字段 “C” 上没有创建索引
curl http://localhost:8080/noIndex/update/-1