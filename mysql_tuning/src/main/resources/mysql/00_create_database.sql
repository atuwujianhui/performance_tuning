-- 创建数据库，编码 UTF-8
create database performance_tuning default character set utf8 collate utf8_general_ci;

# 创建可以远程访问的root账户
CREATE USER 'root'@'%' IDENTIFIED WITH 'caching_sha2_password' BY 'root.123456';
GRANT ALL ON *.* TO 'root'@'%';