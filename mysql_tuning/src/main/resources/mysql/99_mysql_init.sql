-- 创建数据库，编码 UTF-8
create database performance_tuning default character set utf8 collate utf8_general_ci;

-- 示例程序
CREATE TABLE `performance_tuning`.`payment`  (
    `id` bigint(0) NOT NULL AUTO_INCREMENT,
    `serial` varchar(64),
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

-- 创建表 t_no_index
CREATE TABLE performance_tuning.t_tuning (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `b` int(11) DEFAULT NULL,
    `c` int(11) DEFAULT NULL,
    `d` varchar(16) DEFAULT NULL,
    INDEX id_b (`b`),
    INDEX id_d (`d`),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- 创建存储过程，造数 50 万条
delimiter ;;
create procedure performance_tuning.idata()
begin
    declare i int;
    set i = 1;
    SET autocommit = 0;
    while(i <= 500000) do
        insert into performance_tuning.t_tuning(b, c, d) values(i, i, i);
        -- 每插入 100,00 次提交一次
        IF MOD(i, 10000) = 0 THEN
            COMMIT;
END IF;
        set i = i + 1;
end while;
COMMIT;
SET autocommit = 1;
end;;
delimiter ;

-- 执行存储过程
call performance_tuning.idata();