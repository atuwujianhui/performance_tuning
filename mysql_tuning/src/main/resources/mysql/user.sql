-- 以下命令请供参考，请自行根据实际需求创建和修改用户密码和加密方式

-- MySQL 5.7，默认加密方式为 “mysql_native_password”
ALTER USER 'root'@'localhost' IDENTIFIED WITH 'mysql_native_password' BY 'root.123456';
ALTER USER 'root'@'%' IDENTIFIED WITH 'mysql_native_password ' BY 'root.123456';
CREATE USER 'root'@'%' IDENTIFIED WITH 'mysql_native_password ' BY 'root.123456';
GRANT ALL ON *.* TO 'root'@'%';

-- MySQL 8.0，默认加密方式为 “caching_sha2_password”
ALTER USER 'root'@'localhost' IDENTIFIED WITH 'caching_sha2_password' BY 'root.123456';
ALTER USER 'root'@'%' IDENTIFIED WITH 'caching_sha2_password' BY 'root.123456';
CREATE USER 'root'@'%' IDENTIFIED WITH 'caching_sha2_password' BY 'root.123456';
GRANT ALL ON *.* TO 'root'@'%';