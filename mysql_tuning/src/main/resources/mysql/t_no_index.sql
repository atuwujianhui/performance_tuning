-- 创建表 t_no_index
CREATE TABLE performance_tuning.t_no_index (
                            `id` int(11) NOT NULL,
                            `c` int(11) DEFAULT NULL,
                            PRIMARY KEY (`id`)
       ) ENGINE=InnoDB;

-- 创建存储过程，造数 10 万条
delimiter ;;
create procedure performance_tuning.idata()
begin
    declare i int;
    set i = 1;
    SET autocommit = 0;
    while(i <= 500000) do
        insert into performance_tuning.t_no_index values(i, i);
        -- 每插入 100,00 次提交一次
        IF MOD(i, 10000) = 0 THEN
            COMMIT;
        END IF;
        set i = i + 1;
    end while;
    COMMIT;
    SET autocommit = 1;
end;;
delimiter ;

-- 执行存储过程
call performance_tuning.idata();