
VA_OPTS="-Dfile.encoding=utf-8 -Duser.timezone=UTC"
JAVA_TOOL_OPTIONS="-javaagent:/skywalking/agent/skywalking-agent.jar"
SW_AGENT_NAMESPACE="performance"
SW_AGENT_NAME="performance::tuning"
SW_AGENT_COLLECTOR_BACKEND_SERVICES="oap.svc.cluster.local:11800"

java -javaagent:/usr/local/skywalking-agent/skywalking-agent.jar \
  -Dskywalking.agent.service_name=mysql-tuning -Dskywalking.collector.backend_service=oap.skywalking.svc.cluster.local:11800 \
  -Djava.security.egd="file:/dev/./urandom" \
  -jar /usr/app/mysql_tuning.jar \
  --server.port="8080" \
  --spring.datasource.url="jdbc:mysql://gz-cdb-035ob96p.sql.tencentcdb.com:24482/performance_tuning?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true" \
  --spring.datasource.username="root" \
  --spring.datasource.password="root.123456"


java -javaagent:/skywalking/agent/skywalking-agent.jar \
  -Dskywalking.agent.service_name=mysql-tuning -Dskywalking.collector.backend_service=oap.skywalking.svc.cluster.local:11800 \
  -Djava.security.egd="file:/dev/./urandom" \
  -jar /usr/app/mysql_tuning.jar \
  --server.port="8088" \
  --spring.datasource.url="jdbc:mysql://gz-cdb-035ob96p.sql.tencentcdb.com:24482/performance_tuning?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true" \
  --spring.datasource.username="root" \
  --spring.datasource.password="root.123456"