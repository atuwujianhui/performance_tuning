# 创建 Namespace
kubectl create ns skywalking
# 创建 ConfigMap，用于保存 mysql 驱动程序
kubectl create cm mysql-java-client --from-file=/root/k8s/mysql-connector-java-5.1.47.jar -n skywalking
# 部署 Skywalking
kubectl apply -f skywalking.yml


# 验证 Skywalking 是否部署成功（Skywalkng Web UI）
http://119.91.236.18:31080

# 参考资料
https://blog.csdn.net/python2007cn/article/details/130159716