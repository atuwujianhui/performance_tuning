# kube-prometheus 地址
https://github.com/prometheus-operator/kube-prometheus

cd /root/k8s/kube-prometheus-release-0.11/
sed -i s#k8s.gcr.io/kube-state-metrics/kube-state-metrics:v2.4.2#bitnami/kube-state-metrics:latest#g  manifests/kubeStateMetrics-deployment.yaml
sed -i s#k8s.gcr.io/prometheus-adapter/prometheus-adapter:v0.9.1#v5cn/prometheus-adapter:v0.9.1#g  manifests/prometheusAdapter-deployment.yaml

当前版本：Kubernetes 1.23

1. 下载对应版本的 kube-prometheus

2. cd kube-prometheus

3. kubectl create -f manifests/setup

4. kubectl apply -f manifests/


# ======================= 安装 =======================
kubectl apply --server-side -f manifests/setup
kubectl wait \
	--for condition=Established \
	--all CustomResourceDefinition \
	--namespace=monitoring
kubectl apply -f manifests/

# ======================= 创建 ingress =======================
kubectl apply -f prometheus-ingress.yaml

# ======================= 验证服务 =======================
# Prometheus
curl -H "Host:prometheus.fjzcit.cn" http://10.0.24.14
# Grafana
curl -H "Host:grafana.fjzcit.cn" http://10.0.24.14

# 浏览器访问
vi /etc/hosts
119.91.205.100 grafana.performance-tuning.cn
119.91.205.100 prometheus.performance-tuning.cn

http://grafana.performance-tuning.cn
http://prometheus.performance-tuning.cn


# Prometheus

# Grafana
http://122.152.229.211:3000


# ======================= 卸载 =======================
kubectl delete --ignore-not-found=true -f manifests/ -f manifests/setup




# 参考资料
https://blog.51cto.com/u_12276890/5164323


# +++++++++++++++++++++++ 修改YAML +++++++++++++++++++++

由于网络原因，kube-state-metrics和prometheus-adapter镜像地址，在国内无法下载，因此需要修改以下地址
vi manifests/kubeStateMetrics-deployment.yaml
image: bitnami/kube-state-metrics:2.7.0
vi manifests/prometheusAdapter-deployment.yaml
image: cloveropen/prometheus-adapter:v0.10.0

vi manifests/grafana-deployment.yaml
# image: grafana/grafana:8.5.5
image: grafana/grafana:8.3.3

vi manifests/grafana-service.yaml
type: NodePort
nodePort: 30100

vi manifests/prometheus-service.yaml
type: NodePort
nodePort: 30200

vi manifests/kubeStateMetrics-deployment.yaml
#- image: k8s.gcr.io/kube-state-metrics/kube-state-metrics:v2.3.0
- image: bitnami/kube-state-metrics:1.9.7

vi manifests/prometheusAdapter-deployment.yaml
image: k8s.gcr.io/prometheus-adapter/prometheus-adapter:v0.9.1
image: v5cn/prometheus-adapter:v0.9.1


# +++++++++++++++++++++++ Grafana 监控面板推荐 +++++++++++++++++++++

Grafana 从 Prometheus 数据源读取监控指标并进行图形化展示，可通过以下几种方式进行展示：

1.grafana dashboard模板

Grafana Dashboard提供了很多模板供我们下载使用，针对以上不同维度的监控指标，我们可以自行选择喜欢的模板直接导入Dashboard id使用。

node性能
Dashboard id：8919
Dashboard id：9276

pod性能
Dashboard id：8588

K8S资源
Dashboard id：3119
Dashboard id：6417

注意： 如果Dashboard没有数据，需要具体查看相关指标或标签是否存在，因为有的模板是几年前上传的，没有更新。

# 20240707：Prometheus Operator 抓取外部探针服务
https://www.jianshu.com/p/053aafe8836d（Prometheus Operator 高级配置(服务自动发现、持久化存储)）