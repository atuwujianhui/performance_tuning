# 创建部署
kubectl create deployment nginx --image=nginx
kubectl create deployment nginx-demo --image=nginx:1.22.1
kubectl create deployment nginx-demo --image=nginx:1.22.1 --dry-run -o yaml

# 创建Service，暴露端口
kubectl expose deployment nginx --port=80 --type=NodePort
kubectl create service clusterip nginx-service-demo --tcp=80:80 --dry-run -o yaml

# 查看 pod 以及服务信息
kubectl get pod,svc

# 部署 Ingress-nginx
https://blog.csdn.net/qq_35583325/article/details/127391207

# 测试
kubectl -n tuning get all
kubectl -n tuning get ingress

----------------- 集群内网访问 -----------------
# 后端服务（mysql-tuning.yml）
curl -H "Host:mysql-tuning.fjzcit.com" http://10.0.24.14/payment/get/1
# 前端服务（nginx-demo.yml）
curl -H "Host:tuning-nginx.fjzcit.com" http://10.0.24.14
# 前后端分离（nginx-demo.yml）
curl -H "Host:tuning-nginx.fjzcit.com" http://10.0.24.14/api/mysql-tuning/payment/get/1
---------------------------------------------

------------------ 外网访问 ------------------
# 前端服务（nginx-tuning.yml）
curl http://119.91.205.100:30180
# 后端服务（mysql-tuning.yml）
curl http://119.91.205.100:30080/payment/get/1
# Skywalking
http://119.91.236.18:31080

# 修改：C:\Windows\System32\drivers\etc\HOSTS，添加如下映射：
119.91.205.100 mysql-tuning.performance-tuning.com grafana.performance-tuning.cn prometheus.performance-tuning.cn

http://grafana.performance-tuning.cn
http://prometheus.performance-tuning.cn
http://skywalking.performance-tuning.cn/
http://mysql-tuning.performance-tuning.com/payment/get/1
---------------------------------------------

kubectl create cm startup-sh-cm --from-file=startup.sh -n tuning

kubectl get pod -n tuning | grep mysql-tuning | awk '{print $1}' | xargs -n 1 -I pod_name kubectl logs -f pod_name -n tuning

kubectl get pod -n tuning | grep mysql-tuning | awk '{print $1}' | xargs -n 1 -I pod_name kubectl exec -it pod_name -n tuning -- /bin/bash

kubectl exec -it mysql-tuning-deploy-79cb6b5dcb-vpw6t -c mysql-tuning- -n tuning -- /bin/sh

java -javaagent:/skywalking/agent/skywalking-agent.jar -Dskywalking.agent.service_name=mysql-tuning -Dskywalking.collector.backend_service=oap.skywalking.svc.cluster.local:11800 -Djava.security.egd=file:/dev/./urandom -jar /usr/app/mysql_tuning.jar --server.port=8088 --spring.datasource.url=jdbc:mysql://gz-cdb-035ob96p.sql.tencentcdb.com:24482/performance_tuning?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true --spring.datasource.username=root --spring.datasource.password=root.123456