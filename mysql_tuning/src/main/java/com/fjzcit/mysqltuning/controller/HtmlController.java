package com.fjzcit.mysqltuning.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Atu
 * @Date 2024/5/25 8:32
 * @Version 1.0
 */
@Controller
@RequestMapping(value = "/html")
public class HtmlController {

    @GetMapping(value = "/hello")
    public String hello(){
        return "hello";
    }
}
