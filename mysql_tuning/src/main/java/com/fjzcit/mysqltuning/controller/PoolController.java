package com.fjzcit.mysqltuning.controller;

import com.fjzcit.mysqltuning.service.PoolService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author Atu
 * @Date 2024/6/15 18:09
 * @Version 1.0
 */
@RestController
@RequestMapping(value = "/pool")
public class PoolController {

    @Resource
    private PoolService poolService;

    // Tomcat 连接池
    @GetMapping(value = "/tomcatPool/{userName}/{timeout}")
    public String tomcatPool(@PathVariable String userName, @PathVariable Long timeout) {
        return this.poolService.tomcatPool(userName, timeout);
    }

    // 数据库连接池
    @GetMapping(value = "/databasePool")
    public String databasePool(){
        return this.poolService.databasePool();
    }
}
