package com.fjzcit.mysqltuning.controller;

import com.fjzcit.mysqltuning.service.JavaCaseService;
import com.fjzcit.mysqltuning.service.impl.JavaCaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author Atu
 * @Date 2024/5/26 15:32
 * @Version 1.0
 */
@RestController
@RequestMapping(value = "/javaCase")
public class JavaCaseController {
    private static final Logger logger = LoggerFactory.getLogger(JavaCaseController.class);

    @Resource
    private JavaCaseService javaCaseService;

    // Java synchronized 关键字
    @GetMapping(value = "/synchronizedCase")
    public String synchronizedCase() {
        return this.javaCaseService.synchronizedCase();
    }

    // 写日志
    @GetMapping(value = "/diskIO")
    public void diskIO() {
        logger.info("测试 log4j2 组件打印 info 日志！");
    }

    // 长事务占用线程池
    @GetMapping(value = "threadPool")
    public void threadPool() {
        this.javaCaseService.threadPool();
    }

}
