package com.fjzcit.mysqltuning.controller;

import com.fjzcit.mysqltuning.service.NoIndexService;
import com.fjzcit.performancetuning.entities.CommonResult;
import com.fjzcit.performancetuning.entities.Tuning;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author Atu
 * @Date 2024/5/18 8:49
 * @Version 1.0
 * @Description 未建索引相关案例
 */
@RestController
@RequestMapping(value = "/noIndex")
public class NoIndexController {
    @Resource
    private NoIndexService noIndexService;

    // 查询未使用索引
    @GetMapping(value = "/findWithoutIndex/{c}")
    public CommonResult findWithoutIndex(@PathVariable("c") Long c) {
        Tuning tuning = noIndexService.findWithoutIndex(c);
        return new CommonResult(200,"查询成功！", tuning);
    }

    // 无索引更新操作导致锁全表
    @GetMapping(value = "/updateWithoutIndex/{c}")
    public CommonResult updateWithoutIndex(@PathVariable("c") Long c) {
        int result = noIndexService.updateWithoutIndex(c);
        return new CommonResult(200,"修改成功！", result);
    }
}
