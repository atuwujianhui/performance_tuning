package com.fjzcit.mysqltuning.controller;

import com.fjzcit.mysqltuning.service.IndexService;
import com.fjzcit.performancetuning.entities.CommonResult;
import com.fjzcit.performancetuning.entities.Tuning;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author Atu
 * @Date 2024/5/18 9:50
 * @Version 1.0
 * @Description 索引相关案例
 */
@RestController
@RequestMapping(value = "/index")
public class IndexController {

    @Resource
    private IndexService indexService;

    // 索引更新
    @GetMapping(value = "/updateWithIndex/{b}")
    public CommonResult updateWithIndex(@PathVariable("b") Long b) {
        int result = this.indexService.updateWithIndex(b);
        return new CommonResult(200,"修改成功！", result);
    }

    // 索引更新加长事务
    @GetMapping(value = "/updateWithLongTransaction/{m}/{n}")
    public CommonResult updateWithLongTransaction(@PathVariable("m") Long m, @PathVariable("n") Long n) {
        int result = this.indexService.updateWithLongTransaction(m, n);
        return new CommonResult(200,"修改成功！", result);
    }

    // 函数运算导致索引失效
    @GetMapping(value = "/findWithDisabledIndex/{d}")
    public CommonResult findWithDisabledIndex(@PathVariable("d") Long d) {
        Tuning tuning = this.indexService.findWithDisabledIndex(d);
        return new CommonResult(200,"查询成功！", tuning);
    }
}
