package com.fjzcit.mysqltuning.dao;

import com.fjzcit.performancetuning.entities.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author Atu
 * @Date 2024/5/18 8:49
 * @Version 1.0
 */
@Mapper
public interface PaymentDao
{
    public int create(Payment payment);

    public Payment getPaymentById(@Param("id") Long id);
}
