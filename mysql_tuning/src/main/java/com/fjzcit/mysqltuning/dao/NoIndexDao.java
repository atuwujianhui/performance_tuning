package com.fjzcit.mysqltuning.dao;

import com.fjzcit.performancetuning.entities.Tuning;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author Atu
 * @Date 2024/5/18 8:50
 * @Version 1.0
 */
@Mapper
public interface NoIndexDao {
    public int updateWithoutIndex(Long c);

    public Tuning findWithoutIndex(Long c);
}
