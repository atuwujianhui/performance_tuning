package com.fjzcit.mysqltuning.dao;

import com.fjzcit.performancetuning.entities.Tuning;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author Atu
 * @Date 2024/5/26 8:25
 * @Version 1.0
 */
@Mapper
public interface IndexDao {
    public int updateWithIndex(Long b);

    public int updateRangeWithIndex(Long m, Long n);

    public Tuning findWithDisabledIndex(Long e);
}
