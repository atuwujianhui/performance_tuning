package com.fjzcit.mysqltuning;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * @Author Atu
 * @Date 2024/5/18 8:55
 * @Version 1.0
 */
@SpringBootApplication
@EnableTransactionManagement
public class MysqlTuningMain implements CommandLineRunner {

    @Resource
    private DataSource dataSource;

    public static void main(String[] args) {
        SpringApplication.run(MysqlTuningMain.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("DATASOURCE = " + this.dataSource.getClass());
    }
}
