package com.fjzcit.mysqltuning.service.impl;

import cn.hutool.core.thread.ThreadUtil;
import com.fjzcit.mysqltuning.service.JavaCaseService;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @Author Atu
 * @Date 2024/5/26 15:33
 * @Version 1.0
 */
@Service
public class JavaCaseServiceImpl implements JavaCaseService {
    @Override
    public String synchronizedCase() {
        return helloSynchronized();
    }

    @Override
    public String threadPool() {
        // try {
        //     // 睡眠时会占用连接池（Connection），但是因为不执行代码，所以不占用线程池（Thread）
        //     Thread.sleep(3000);
        // } catch (InterruptedException e) {
        //     e.printStackTrace();
        // }
        String threadName = Thread.currentThread().getName();
        System.out.println("线程：" + threadName);
        //故意设置睡眠10分钟，模拟线程被占用了！
        ThreadUtil.sleep(10, TimeUnit.MINUTES);
        return "线程：" + threadName;
    }

    private synchronized String helloSynchronized() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "Hello, ThreadID - " + Thread.currentThread().getId();
    }
}
