package com.fjzcit.mysqltuning.service.impl;

import com.fjzcit.mysqltuning.dao.NoIndexDao;
import com.fjzcit.mysqltuning.service.NoIndexService;
import com.fjzcit.performancetuning.entities.CommonResult;
import com.fjzcit.performancetuning.entities.Tuning;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @Author Atu
 * @Date 2024/5/18 9:53
 * @Version 1.0
 */
@Service
public class NoIndexServiceImpl implements NoIndexService {

    @Resource
    private NoIndexDao noIndexDao;

    @Override
    public int updateWithoutIndex(Long c) {
        return this.noIndexDao.updateWithoutIndex(c);
    }

    @Override
    public Tuning findWithoutIndex(Long c) {
        return this.noIndexDao.findWithoutIndex(c);
    }
}
