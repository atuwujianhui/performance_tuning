package com.fjzcit.mysqltuning.service.impl;

import cn.hutool.core.thread.ThreadUtil;
import com.fjzcit.mysqltuning.service.PoolService;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * @Author Atu
 * @Date 2024/6/15 18:09
 * @Version 1.0
 */
@Service
public class PoolServiceImpl implements PoolService  {

    // Tomcat 连接池配置
    @Override
    public String tomcatPool(String userName, Long timeout) {
        Calendar calendar= Calendar.getInstance();
        String currentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
        String threadName = Thread.currentThread().getName();
        String summary = "执行时间：" + currentTime + " | 用户名：" + userName + " | 执行线程：" + threadName;
        System.out.println(summary);
        //设置睡眠X毫秒，模拟线程被占用了！
        ThreadUtil.sleep(timeout, TimeUnit.MILLISECONDS);
        return summary;
    }

    // 数据库连接池配置
    @Override
    public String databasePool() {

        return getMessage();
    }

    private String getMessage() {
        return "Hello, ThreadID - " + Thread.currentThread().getId();
    }
}
