package com.fjzcit.mysqltuning.service;

import org.springframework.stereotype.Service;

/**
 * @Author Atu
 * @Date 2024/6/15 18:09
 * @Version 1.0
 */
public interface PoolService {
    public String tomcatPool(String userName, Long timeout);
    public String databasePool();
}
