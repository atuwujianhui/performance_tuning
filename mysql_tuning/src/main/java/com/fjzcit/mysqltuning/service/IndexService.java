package com.fjzcit.mysqltuning.service;

import com.fjzcit.performancetuning.entities.Tuning;

/**
 * @Author Atu
 * @Date 2024/5/26 8:22
 * @Version 1.0
 */
public interface IndexService {
    public int updateWithIndex(Long b);
    public int updateRangeWithIndex(Long m, Long n);
    public int updateWithLongTransaction(Long m, Long n);


    public Tuning findWithDisabledIndex(Long e);
}
