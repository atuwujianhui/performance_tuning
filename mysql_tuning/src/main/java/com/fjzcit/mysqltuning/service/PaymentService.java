package com.fjzcit.mysqltuning.service;

import com.fjzcit.performancetuning.entities.Payment;
import org.apache.ibatis.annotations.Param;

/**
 * @Author Atu
 * @Date 2024/5/18 8:49
 * @Version 1.0
 */
public interface PaymentService
{
    public int create(Payment payment);

    public int autoCreate();

    public Payment getPaymentById(@Param("id") Long id);


}
