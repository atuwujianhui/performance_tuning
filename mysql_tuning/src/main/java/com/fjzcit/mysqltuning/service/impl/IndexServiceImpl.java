package com.fjzcit.mysqltuning.service.impl;

import com.fjzcit.mysqltuning.dao.IndexDao;
import com.fjzcit.mysqltuning.service.IndexService;
import com.fjzcit.performancetuning.entities.Tuning;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @Author Atu
 * @Date 2024/5/26 8:23
 * @Version 1.0
 */
@Service
public class IndexServiceImpl implements IndexService {

    @Resource
    private IndexDao indexDao;

    @Override
    public int updateWithIndex(Long b) {
        return this.indexDao.updateWithIndex(b);
    }

    @Override
    public int updateRangeWithIndex(Long m, Long n) {
        return this.indexDao.updateRangeWithIndex(m, n);
    }

    @Override
    @Transactional
    public int updateWithLongTransaction(Long m, Long n) {
        // 更新加锁
        int result = this.indexDao.updateRangeWithIndex(m, n);
        // 执行其他业务耗时 60 秒
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Tuning findWithDisabledIndex(Long e) {
        return this.indexDao.findWithDisabledIndex(e);
    }
}
