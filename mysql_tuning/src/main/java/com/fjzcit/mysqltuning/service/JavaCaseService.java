package com.fjzcit.mysqltuning.service;

/**
 * @Author Atu
 * @Date 2024/5/26 15:33
 * @Version 1.0
 */
public interface JavaCaseService {
    public String synchronizedCase();
    public String threadPool();
}
