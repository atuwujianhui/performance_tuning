package com.fjzcit.mysqltuning.service;

import com.fjzcit.performancetuning.entities.CommonResult;
import com.fjzcit.performancetuning.entities.Tuning;

/**
 * @Author Atu
 * @Date 2024/5/18 8:50
 * @Version 1.0
 */
public interface NoIndexService {
    public int updateWithoutIndex(Long c);

    public Tuning findWithoutIndex(Long c);
}
