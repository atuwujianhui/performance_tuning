package com.fjzcit.mysqltuning.service.impl;

import com.fjzcit.mysqltuning.dao.PaymentDao;
import com.fjzcit.mysqltuning.service.PaymentService;
import com.fjzcit.performancetuning.entities.Payment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @Author Atu
 * @Date 2024/5/18 8:49
 * @Version 1.0
 */
@Service
public class PaymentServiceImpl implements PaymentService
{
    @Resource
    private PaymentDao paymentDao;

    public int create(Payment payment)
    {
        return paymentDao.create(payment);
    }

    @Override
    public int autoCreate() {
        Payment payment = new Payment();
        payment.setSerial(UUID.randomUUID().toString());
        return paymentDao.create(payment);
    }

    public Payment getPaymentById(Long id)
    {
        return paymentDao.getPaymentById(id);
    }
}
