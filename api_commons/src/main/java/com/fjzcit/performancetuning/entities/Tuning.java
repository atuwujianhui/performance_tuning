package com.fjzcit.performancetuning.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Atu
 * @Date 2024/5/26 10:55
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tuning {
    private Long id;
    private Long b;
    private Long c;
    private Long d;
}
